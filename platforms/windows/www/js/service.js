﻿app.service('sharedData', function () {

    //search string
    var searchString = '';

    var stringEnumerator = null;
    var stringAlarmId = null;
    var stringAlarmName = null;
    var scrollPos = null;

    var recentlyViewed = false;

    return {
        setRecentlyViewed: function (v) {
            recentlyViewed = v;
        },
        isRecentlyViewed: function () {
            return recentlyViewed;
        },
        setScrollPos: function (value) {
            scrollPos = value;
        },
        getScrollPos: function () {
            return scrollPos;
        },
        getEnumerator: function () {
            return stringEnumerator;
        },
        setEnumerator: function (value) {
            stringEnumerator = value;
        },
        getAlarmId: function () {
            return stringAlarmId;
        },
        setAlarmId: function (value) {
            stringAlarmId = value;
        },

        //set/get search string
        getSearchVal: function () {
            return searchString;
        },
        setSearchVal: function (value) {
            searchString = value;
        },
        setAlarmName: function (value) {
            stringAlarmName = value;
        },

        getAlarmName: function () {
            return stringAlarmName;
        }
    };
});