﻿app.factory("alarmRequest", function ($http, $rootScope, sharedData, $q) {
    return {
        request: function () {

            var verObjShortName = $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName;

            var langObjLangCode = $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].languages[$rootScope.langIndex].langCode;

            //request the alarms
            var p1 = $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + verObjShortName + "/" + verObjShortName + "_alarms_" + langObjLangCode + ".json");

            //request static strings
            var p2 = $http.get("data/strings_" + langObjLangCode + ".json");

            //merge the two and return them
            return $q.all([p1, p2]).then(function (data) {

                var currentAlarmObj = data[0].data.alarms.filter(function (x) {
                    return x.alarmId == sharedData.getAlarmId();
                });

                /*                //if optia
                                if ($rootScope.masterObj[$rootScope.prodIndex].shortName == 'optia') {

                                    //loop thru obj and get actions
                                    var arr = [];
                                    var c = 0;
                                    angular.forEach(currentAlarmObj[0], function (v, i) {
                                        var obj = {
                                            cause: null,
                                            action: null
                                        };
                                        if (i.indexOf("action") !== -1) {
                                            obj.action = v;
                                            obj.cause = currentAlarmObj[0]["cause" + c];
                                            arr.push(obj);
                                            //delete currentAlarmObj[0][i];
                                            //delete currentAlarmObj[0]["cause" + c];
                                            c++;
                                        }
                                    });

                                    angular.extend(currentAlarmObj[0], {
                                        causesActions: arr
                                    });
                                }*/

                return [currentAlarmObj[0], data[1].data.strings];
            });
        }
    };
});

app.factory("masterRequest", function ($http) {
    return {
        request: function () {
            return $http.get('data/products.json').success(function (r1) {
                angular.forEach(r1.prodList, function (v1, i1) {
                    $http.get("data/" + v1.shortName + "/" + v1.shortName + "_versions.json").success(function (r2) {
                        angular.extend(r1.prodList[i1], {
                            versions: r2.verList,
                            idx: i1
                        });
                        angular.forEach(r2.verList, function (v2, i2) {
                            $http.get("data/" + r1.prodList[i1].shortName + "/" + v2.shortName + "/" + v2.shortName + "_languages.json").success(function (r3) {
                                angular.forEach(r3.langList, function (v3, i3) {
                                    angular.extend(v3, {
                                        idx: i3
                                    });
                                });
                                angular.extend(v2, {
                                    languages: r3.langList,
                                    idx: i2
                                });
                            });
                        });
                    });
                });
                return r1;
            });
        }
    };
});