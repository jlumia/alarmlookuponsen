﻿// convert /n to <br>
app.filter("nl2br", function () {
    return function (data) {
        if (!data) {
            return data;
        }
        return data.replace(/\n\r?/g, '<br>');
    };
});

app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    };
});

// filter by search strings
app.filter("alarmNameFilter", function () {
    return function (alarms, searchFilters, prod) {
        var searchVal = null;
        var filtered = null;
        switch (prod) {
        case "optia":
            searchVal = "enumerator";
            break;
        default:
            searchVal = "log";
            break;
        }
        if (alarms) {
            var f = [];
            angular.forEach(searchFilters, function (v2) {
                if (!v2.checked) {
                    f.push(v2.value);
                }
            });
            filtered = alarms.filter(function (alarm) {
                for (var i in f) {
                    if (alarm[searchVal]) {
                        if (alarm[searchVal].indexOf(f[i]) !== -1) {
                            return false;
                        }
                    }
                }
                return true;
            });
        }
        if (filtered) {
            return filtered;
        }
        return alarms;
    };
});