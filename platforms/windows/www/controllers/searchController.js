﻿app.controller('searchController', function ($scope, sharedData, $http, $rootScope, $timeout, $window) {
    ons.ready(function () {

        //prevLocation
        $rootScope.prevLocation = true;

        if (!$rootScope.changeView) {
            ons.createPopover('templates/popovers/changeview.html', {
                parentScope: $scope
            }).then(function (popover) {
                $rootScope.changeView = popover;
                $rootScope.$digest();
                if (!$rootScope.settingsObj) {
                    var t = $timeout(function () {
                        popover.show(".changeView");
                        $timeout.cancel(t);
                    }, 500);
                }
            });
        }

        //update search filter toggles
        $scope.updateLocalStorage = function () {
            $window.localStorage.setItem('searchFilters', JSON.stringify($rootScope.searchFilters));
        };

        //if ($rootScope.requestedLangIndex && $rootScope.langIndex) {
        if ($rootScope.requestedLangIndex !== $rootScope.langIndex) {
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "_alarmNames_" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].languages[$rootScope.langIndex].langCode + ".json").success(function (response) {

                //assign new alarms
                $rootScope.alarmNames = response.nameList;

                //set scroll top
                $rootScope.$watch('alarmNames', function () {
                    sharedData.setScrollPos(0);
                });

                //reset limit number
                $rootScope.limitNumber = 50;

                //assign new language
                $rootScope.requestedLangIndex = $rootScope.langIndex;

            });
        } else {

            //scroll to position
            $rootScope.$watch('alarmNames', function () {
                var t = $timeout(function () {
                    $rootScope.$broadcast("setscrollpos");
                    $timeout.cancel(t);
                }, 1000);
            });

        }
        //}
        $scope.changeV = function (p, v, l) {
            if ($rootScope.prodIndex === p.idx && $rootScope.verIndex === v.idx && $rootScope.langIndex === l.idx) {
                return;
            }
            $rootScope.prodIndex = p.idx;
            $rootScope.verIndex = v.idx;
            $rootScope.langIndex = l.idx;

            $rootScope.saveSettings(p.idx, v.idx, l.idx);

            $scope.search = '';

            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName + "_alarmNames_" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].languages[$rootScope.langIndex].langCode + ".json").success(function (response) {

                //assign new alarms
                $rootScope.alarmNames = response.nameList;

                //set scroll top
                $rootScope.$watch('alarmNames', function () {
                    sharedData.setScrollPos(0);
                });

                //reset limit number
                $rootScope.limitNumber = 50;

                //assign new language
                $rootScope.requestedLangIndex = l.idx;

            });

        };

        //click on each alarm
        $scope.alarmBool = false;
        $scope.alarmClick = function (x, s) {

            if (!$scope.alarmBool) {
                $scope.alarmBool = true;
                //set enumerator and alarm id and alarm name
                sharedData.setEnumerator(x.enumerator);
                sharedData.setAlarmId(x.alarmId);
                sharedData.setAlarmName(x.name);

                //set search val
                sharedData.setSearchVal(s);

                //set scroll pos
                $scope.$broadcast("getscrollpos");
                $rootScope.forward("alarm");
            }
        };

        //get search value
        $scope.search = sharedData.getSearchVal();

    });
});