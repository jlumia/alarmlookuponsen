app.controller('alarmController', function ($scope, sharedData, alarmRequest, $rootScope, $http) {
    ons.ready(function () {

        $scope.backBool = false;
        $scope.backSearch = function () {
            if (!$scope.backBool) {
                $scope.backBool = true;
                $rootScope.back();
            }
        };

        //alarm name
        $scope.stringAlarmName = sharedData.getAlarmName();

        //get correct alarm troubleshooting
        $scope.stringEnumerator = sharedData.getEnumerator();
        //$scope.stringTsId = sharedData.getTSID();

        //alarm filter
        $scope.stringAlarmId = sharedData.getAlarmId();

        //request current alarm
        alarmRequest.request().then(function (response) {
            $rootScope.currentAlarm = response;
        });

        if (!$rootScope.langPopover) {
            ons.createPopover('templates/popovers/lang.html').then(function (popover) {
                $rootScope.langPopover = popover;
            });
        }

        //passing in the key name "alarmId", the current alarmId, and the troubleshooting array of objects. 
        $scope.filterTroubleshooting = function (filterValue, filterField, data) {
            //filter is a javascript method for filtering an array, think of filter as a for loop, passes all the elements that match the filter criteria and creates a new array
            //rips through data and returns the object that matches the filterField.

            if ($rootScope.masterObj[$rootScope.prodIndex].shortName === "mirasol") {

                var n = filterField.match(/\d/g);
                filterField = filterField.substring(0, filterField.lastIndexOf(n[n.length - 1]) + 1);

            }

            var a = data.filter(function (x) {
                return x[filterValue] == filterField;
            });
            //we have to tell it to get the first value out of the array, even though there is only one value.
            return a[0];
        };

        //isString
        $scope.isString = angular.isString;

        switch ($rootScope.masterObj[$rootScope.prodIndex].shortName) {
        case "trima":
            var search = $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName.search(/\d/);
            //get trouble shooting +search+ is a cool way to concat the search variable into the string
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "_troubleshooting_" + $rootScope.masterObj[$rootScope.prodIndex].versions[$rootScope.verIndex].shortName[search] + ".json").success(function (data) {
                $scope.troubleshooting = $scope.filterTroubleshooting("alarmId", sharedData.getAlarmId(), data.troubleshooting);
            });
            break;
        case "optia":
            //get trouble shooting
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "_troubleshooting.json").success(function (data) {
                $scope.troubleshooting = $scope.filterTroubleshooting("enumerator", sharedData.getEnumerator(), data.troubleshooting);
            });
            //get occurs
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "_occurs.json").success(function (data) {
                $scope.occurs = $scope.filterTroubleshooting("enumerator", sharedData.getEnumerator(), data.occursList);
            });
            break;
        case "quantum":
            //get quantum info
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "_info.json").success(function (data) {
                $scope.info = $scope.filterTroubleshooting("enumerator", sharedData.getEnumerator(), data.info);
            });
            break;
        default:
            //get trouble shooting
            //passing the response to $scope.filterTroubleshooting with arugments: "alarmId" goes to filterValue, sharedData.getAlarmId goes to filterField, data.troubleshooting goes to data. The order of arguments is significant.
            $http.get("data/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "/" + $rootScope.masterObj[$rootScope.prodIndex].shortName + "_troubleshooting.json").success(function (data) {
                $scope.troubleshooting = $scope.filterTroubleshooting("alarmId", sharedData.getAlarmId(), data.troubleshooting);
            });
        }

    });
});