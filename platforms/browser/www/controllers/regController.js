app.controller('regController', function ($scope, $http, masterRequest, $rootScope, $window, $timeout) {
    ons.ready(function () {

        if ($rootScope.prevLocation) {
            $scope.t2 = $timeout(function () {
                $scope.done();
            }, 500);
        }

        $scope.back = function () {
            angular.element(document.getElementsByClassName("tab-bar--material__item")[0]).find("input")[0].checked = true;
            carousel.prev();
        };

        $scope.formCopy = angular.copy($rootScope.userInfo);
        $scope.done = function () {
            angular.element(document.getElementsByClassName("tab-bar--material__item")[1]).find("input")[0].checked = true;
            carousel.next();
            if (!angular.equals($rootScope.userInfo, $scope.formCopy)) {
                document.getElementById("userform").submit();
            }
        };

        $scope.showTitle = function (p) {
            if (p.checked) {
                p.showProd = !p.showProd;
            }
        };

        $scope.pChecker = function (p) {
            p.checked = !p.checked;
            if (!p.checked) {
                p.showProd = false;
                angular.forEach(p.versions, function (v) {
                    if (v.checked) {
                        angular.forEach(v.languages, function (v1) {
                            if (v1.checked) {
                                v1.checked = false;
                            }
                        });
                        v.checked = false;
                    }
                });
            } else {
                p.showProd = true;
            }
        };

        $scope.vChecker = function (v) {
            v.checked = !v.checked;
            if (!v.checked) {
                angular.forEach(v.languages, function (v1) {
                    if (v1.checked) {
                        v1.checked = false;
                    }
                });
            }
        };

        $scope.t = $timeout(function () {
            if (!$rootScope.masterObj) {
                masterRequest.request().success(function (response) {
                    $rootScope.masterObj = response.prodList;
                });
            }
            $timeout.cancel($scope.t);
        }, 1000);

        $scope.clear = function (o) {
            angular.forEach(o, function (v1) {
                angular.forEach(v1.versions, function (v2) {
                    angular.forEach(v2.languages, function (v3) {
                        if (v3.checked) {
                            v3.checked = false;
                        }
                        if (v3.showProd) {
                            v3.showProd = false;
                        }
                    });
                    if (v2.checked) {
                        v2.checked = false;
                    }
                    if (v2.showProd) {
                        v2.showProd = false;
                    }
                });
                if (v1.checked) {
                    v1.checked = false;
                }
                if (v1.showProd) {
                    v1.showProd = false;
                }
            });
            $window.localStorage.removeItem("settingsObj");
        };

        $scope.updateBoolean = false;
        $scope.update = function (o) {
            if (!$scope.updateBoolean) {
                $scope.updateBoolean = true;
                $window.localStorage.setItem('registration', JSON.stringify(o));
                $window.localStorage.setItem('userInfo', JSON.stringify($rootScope.userInfo));

                for (var i = 0; i < o.length; i++) {
                    if (o[i].checked) {
                        $rootScope.prodIndex = o[i].idx;
                        for (var j = 0; j < o[i].versions.length; j++) {
                            if (o[i].versions[j].checked) {
                                $rootScope.verIndex = o[i].versions[j].idx;
                                for (var k = 0; k < o[i].versions[j].languages.length; k++) {
                                    if (o[i].versions[j].languages[k].checked) {
                                        $rootScope.langIndex = o[i].versions[j].languages[k].idx;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }

                $rootScope.saveSettings($rootScope.prodIndex, $rootScope.verIndex, $rootScope.langIndex);

                if (!$rootScope.prevLocation) {
                    $rootScope.forward("search");
                } else {
                    $rootScope.back("search");
                }
            }
        };

        $scope.watcher = $scope.$watch("masterObj", function (n) {
            $scope.watcher = false;
            if (n) {
                for (var i = 0; i < n.length; i++) {
                    if (n[i].checked) {
                        var verCount = 0;
                        var langCount = 0;
                        for (var j = 0; j < n[i].versions.length; j++) {
                            if (n[i].versions[j].checked) {
                                verCount++;
                                for (var k = 0; k < n[i].versions[j].languages.length; k++) {
                                    if (n[i].versions[j].languages[k].checked) {
                                        langCount++;
                                    }
                                }
                            }
                        }
                        if (verCount > langCount || verCount === 0) {
                            $scope.watcher = false;
                            break;
                        } else {
                            $scope.watcher = true;
                        }
                    }
                }
            }
        }, true);
    });
});